def shops():
	return {
		1: {
			'title': 'Магазин 24/7',
			'location': 2,
			'type': 1
		}, 2: {
			'title': 'Магазин Drive & Drip',
			'location': 5,
			'type': 1
		}, 3: {
			'title': 'Магазин Drive & Drip',
			'location': 6,
			'type': 1
		}
	}

def shop_types():
	return {
		1: {
			'items': {
				1: {
					'type': 'smartphones',
					'id': 1
				}, 2: {
					'type': 'smartphones',
					'id': 2
				}
			}
		}
	}

def shop_items():
	return {
		'smartphones': {
			1: {
				'title': 'Смартфон Meizu M5s',
				'price': 250
			}, 2: {
				'title': 'Смартфон Xiaomi Redmi 7 Note',
				'price': 550
			}
		}
	}
