def locations():
	return {
		1: {
			'title': 'Автобусный вокзал ЛС',
			'city': 1
		}, 2: {
			'title': 'Мэрия ЛС',
			'city': 1
		}, 3: {
			'title': 'Склад',
			'city': 1
		}, 4: {
			'title': 'Ферма',
			'city': 1
		}, 5: {
			'title': 'Таксопарк ЛС',
			'city': 1
		}, 6: {
			'title': 'Автошкола',
			'city': 2
		}
	}
