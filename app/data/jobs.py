def jobs():
	return {
		0: {
			'title': 'Безработный',
		}, 1: {
			'title': 'Водитель автобуса',
			'reqlvl': 2,
			'payday': 60,
			'location': 1,
			'time': 20
		}, 2: {
			'title': 'Таксист',
			'reqlvl': 2,
			'payday': 25,
			'location': 5,
			'time': 10
		}
	}
