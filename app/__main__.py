import os

from aoiklivereload import LiveReloader

from db import Base, engine
from db.user import User
from bot.commands import register_commands_handlers
from bot.admincmd import register_admincmd_handlers
from bot.text import register_text_handlers
from bot.callbacks import register_callbacks
from bot.bot import bot

os.environ['APP_VERSION'] = '0.1.42_a'

if int(os.environ['APP_DEBUG']) == 1:
	reloader = LiveReloader()
	reloader.start_watcher_thread()

Base.metadata.create_all(engine)

register_commands_handlers(bot)
register_admincmd_handlers(bot)
register_text_handlers(bot)
register_callbacks(bot)

if __name__ == '__main__':
	bot.polling()
