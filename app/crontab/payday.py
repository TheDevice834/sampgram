from datetime import datetime

from db.user import User
from db.system import System

def payday(bot):
	if datetime.now().minute == 00:
		users = User.get_for_payday()
		payday_exp = int(System.get('payday_exp'))
		payday_bonus_cash = int(System.get('payday_bonus_cash'))

		exp = 0
		cash = 0
		reqcash = 200

		for user in users:
			cash = user.job_cash

			if cash >= reqcash:
				exp = payday_exp

			user.add_exp(exp)
			user.cash += cash
			user.cash += payday_bonus_cash
			user.job_cash = 0
			user.commit()

			payday_message = '——— <b>PAYDAY</b> ———\n'
			if exp == 0:
				payday_message += '— Вы не получаете опыт, потому что заработали меньше <b>${0}</b>\n'.format(reqcash)
			if payday_bonus_cash > 0:
				payday_message += '— В качестве бонуса от администрации Вы получаете <b>${0}</b>\n'.format(payday_bonus_cash)
			payday_message += '— Получено опыта: <b>{0} exp</b>\n'.format(exp)
			payday_message += '— Зарплата: <b>${0}</b>'.format(cash)

			try:
				bot.send_message(user.t_id, payday_message, parse_mode='html')
			except:
				user.delete()
				print('Blocked')
