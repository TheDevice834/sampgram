from db.user import User
from db.job import Job

from data.shops import shops as get_shops

def locations(bot):
	users = User.get_for_update_locations()

	for user in users:
		user.location_move -= 1
		user.commit()

		if user.location_move == 0:
			bot.send_message(user.t_id, 'Вы добрались до локации <b>{0}</b>'.format(User.get_locations()[user.location]['title']), parse_mode='html')

			shops = get_shops()
			loc_shops = {}
			shops_text = ''

			for shop in shops:
				if shops[shop]['location'] == user.location:
					if shops_text == '':
						shops_text += 'На данной локации есть магазины:'
					shops_text += '\n— <b>{0}</b>'.format(shops[shop]['title'])

			if shops_text != '':
				bot.send_message(user.t_id, shops_text, parse_mode='html')

			if user.location == 6 and user.a_lic == 0:
				bot.send_message(user.t_id, '— Для получения водительского удостоверения введите команду <b>/aschool</b>', parse_mode='html')
			elif user.location == 3:
				job = Job.get_by_job_id(1)

				text = '— Состояние склада: <b>{0}</b> материалов'.format(job.items)

				if job.owner_id:
					text += '\n— Владелец склада: <b>@{0}</b>'.format(User.get(job.owner_id).t_username)

				bot.send_message(user.t_id, text, parse_mode='html')
