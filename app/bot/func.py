from datetime import datetime

from db.user import User
from db.job import Job

from data.shops import shop_items

def show_profile(bot, chat_id, user):
	profile_message = 'Аккаунт <b>@{0} (ID: {1})</b>\n'.format(user.t_username, user.id)

	profile_message += '— Деньги: <b>${0}</b>\n'.format(user.cash)
	profile_message += '— Уровень: <b>{0} ({1}/{2})</b>\n'.format(user.level, user.exp, user.count_max_exp())
	profile_message += '— Работа: <b>{0}</b>\n'.format(User.get_jobs()[user.job]['title'])
	if user.smartphone != 0:
		profile_message += '— <b>{0}</b>\n'.format(shop_items()['smartphones'][user.smartphone]['title'])
	if user.location_move == 0:
		profile_message += '— Вы на локации: <b>{0}</b>'.format(User.get_locations()[user.location]['title'])
	else:
		profile_message += '— Вы передвигаетесь на локацию: <b>{0}</b>'.format(User.get_locations()[user.location]['title'])

	bot.send_message(chat_id, profile_message, parse_mode='html')

def show_jobs(bot, chat_id, user):
	jobs = User.get_jobs()
	del jobs[0]

	jobs_list = 'Список работ:\n'
	for job in jobs:
		jobs_list += '– <b>{0}</b>\n'.format(jobs[job]['title'])
		jobs_list += '(зарплата: <b>${0} за {1} сек</b>, требуемый уровень: <b>{2}</b>)\n'.format(jobs[job]['payday'], jobs[job]['time'], jobs[job]['reqlvl'])

	bot.send_message(chat_id, jobs_list, parse_mode='html')

def do_work(bot, chat_id, user):
	if user.check_location(3) or user.check_location(4):
		if user.check_location(3):
			job = 1
			payday = 10
			time = 2
		elif user.check_location(4):
			job = 2
			payday = 4
			time = 1
		else:
			return False

		can_work = user.can_work(time)
		if can_work != True:
			return bot.send_message(chat_id, 'Вы сможете работать через <b>{0}</b> сек.'.format(can_work), parse_mode='html')

		user.cash += payday
		user.job_timestamp = datetime.now().timestamp()
		user.commit()

		if job == 1:
			job = Job.get_by_job_id(1)
			job.items += 1
			job.commit()

		return bot.send_message(chat_id, 'Вы получили <b>${0}</b>\nВаш баланс: <b>${1}</b>'.format(payday, user.cash), parse_mode='html')
	if user.job == 0:
		return bot.send_message(chat_id, 'У Вас нет работы!\nДля того, чтобы устроиться перейдите на локацию <b>Мэрия ЛС</b> и введите <b>/getjob</b>\nИли Вы можете заняться подработкой на Ферме или Складе', parse_mode='html')
	work_location = User.get_jobs()[user.job]['location']

	if not user.check_location(work_location):
		return bot.send_message(chat_id, 'Для работы <b>{0}</b> перейдите на локацию <b>{1}</b>'.format(User.get_jobs()[user.job]['title'], User.get_locations()[work_location]['title']), parse_mode='html')

	can_work = user.can_work()
	if can_work != True:
		return bot.send_message(chat_id, 'Вы сможете работать через <b>{0}</b> сек.'.format(can_work), parse_mode='html')

	user.job_cash += User.get_jobs()[user.job]['payday']
	user.job_timestamp = datetime.now().timestamp()
	user.commit()

	bot.send_message(chat_id, 'Вы получили <b>${0}</b>\nОбщий заработок: <b>${1}</b>'.format(User.get_jobs()[user.job]['payday'], user.job_cash), parse_mode='html')
