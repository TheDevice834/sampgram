import os

from db.user import User
from db.system import System

def register_admincmd_handlers(bot):
	@bot.message_handler(commands=['payday'])
	def payday(message):
		a_user = User.get_by_t_id(message.from_user.id)
		if a_user and a_user.admin == 1:
			args = message.text.split()[1:]

			if len(args) != 2:
				return bot.send_message(message.chat.id, 'Используйте: /payday <exp> <bonus_cash>')

			exp = args[0]
			bonus_cash = args[1]

			System.set('payday_exp', exp)
			System.set('payday_bonus_cash', bonus_cash)
			text = 'Вы установили <b>получаемый опыт</b> во время PAYDAY: <b>{0} exp</b>'.format(exp)
			text += '\nВы установили <b>бонусные деньги</b> во время PAYDAY: <b>${0}</b>'.format(bonus_cash)

			return bot.send_message(message.chat.id, text, parse_mode='html')

	@bot.message_handler(commands=['setcash'])
	def setcash(message):
		a_user = User.get_by_t_id(message.from_user.id)
		if a_user and a_user.admin == 1:
			args = message.text.split()[1:]

			if len(args) != 2:
				return bot.send_message(message.chat.id, 'Используйте: /setcash <username> <cash>')

			username = args[0]
			cash = int(args[1])

			user = User.get_by_t_username(username)
			if user:
				user.cash = cash
				user.commit()
				bot.send_message(message.chat.id, 'Вы установили баланс в <b>${0}</b> игроку <b>@{1}</b>'.format(cash, username), parse_mode='html')
				bot.send_message(user.t_id, 'Администратор <b>@{0}</b> установил Вам баланс в <b>${1}</b>'.format(a_user.t_username, cash), parse_mode='html')
			else:
				bot.send_message(message.chat.id, 'Игрок <b>@{0}</b> не найден'.format(username), parse_mode='html')

	@bot.message_handler(commands=['setlevel'])
	def setlevel(message):
		a_user = User.get_by_t_id(message.from_user.id)
		if a_user and a_user.admin == 1:
			args = message.text.split()[1:]

			if len(args) != 2:
				return bot.send_message(message.chat.id, 'Используйте: /setlevel <username> <level>')

			username = args[0]
			level = int(args[1])

			user = User.get_by_t_username(username)
			if user:
				user.level = level
				user.commit()
				bot.send_message(message.chat.id, 'Вы установили <b>{0}-й</b> уровень игроку <b>@{1}</b>'.format(level, username), parse_mode='html')
				bot.send_message(user.t_id, 'Администратор <b>@{0}</b> установил Вам <b>{1}-й</b> уровень'.format(a_user.t_username, level), parse_mode='html')
			else:
				bot.send_message(message.chat.id, 'Игрок <b>@{0}</b> не найден'.format(username), parse_mode='html')

	@bot.message_handler(commands=['tp'])
	def tp(message):
		a_user = User.get_by_t_id(message.from_user.id)
		if a_user and a_user.admin == 1:
			args = message.text.split()[1:]

			if len(args) != 2:
				return bot.send_message(message.chat.id, 'Используйте: /tp <username> <to>')

			username = args[0]
			to = int(args[1])

			user = User.get_by_t_username(username)
			if user:
				user.location = to
				user.location_move = 0
				user.commit()
				bot.send_message(message.chat.id, 'Вы телепортировали игрока <b>@{1}</b> на локацию <b>{0}</b>'.format(User.get_locations()[user.location]['title'], username), parse_mode='html')
				bot.send_message(user.t_id, 'Администратор <b>@{0}</b> телепортировал Вас на локацию <b>{1}</b>'.format(a_user.t_username, User.get_locations()[user.location]['title']), parse_mode='html')
			else:
				bot.send_message(message.chat.id, 'Игрок <b>@{0}</b> не найден'.format(username), parse_mode='html')

	@bot.message_handler(commands=['message'])
	def adminmessage(message):
		a_user = User.get_by_t_id(message.from_user.id)
		if a_user and a_user.admin == 1:
			bot.send_message(message.chat.id, 'Введите текст сообщения\nОно будет отправлено всем игрокам')
			os.environ['{0}_TEXT_HANDLER'.format(message.from_user.id)] = 'adminmessage'
