import os

from telebot import types

from bot import func
from db.user import User
from db.job import Job
from data.shops import shops as get_shops

def register_commands_handlers(bot):
	@bot.message_handler(commands=['start'])
	def start_message(message):
		user = User.get_by_t_id(message.from_user.id)

		if not user:
			if not message.from_user.username:
				username = 'user{0}'.format(message.chat.id)
			else:
				username = message.from_user.username

			welcome_message = 'Добро пожаловать в <b>SAMPGRAM</b>!\n'
			welcome_message += 'Это <b>SAMP-симулятор</b> в виде бота для <b>Telegram</b>.\n\n'
			welcome_message += 'Хотите создать аккаунт?'

			keyboard = types.ReplyKeyboardMarkup(True)
			keyboard.row('Да', 'Нет')

			bot.send_message(message.chat.id, welcome_message, parse_mode='html', reply_markup=keyboard)
		else:
			bot.send_message(message.chat.id, 'Вы уже зарегистрированы!')

	@bot.message_handler(commands=['profile'])
	def profile_command(message):
		user = User.get_by_t_id(message.from_user.id)
		if user:
			func.show_profile(bot, message.chat.id, user)

	@bot.message_handler(commands=['jobs'])
	def jobs_command(message):
		user = User.get_by_t_id(message.from_user.id)
		if user:
			func.show_jobs(bot, message.chat.id, user)

	@bot.message_handler(commands=['getjob'])
	def get_job(message):
		user = User.get_by_t_id(message.from_user.id)
		if user:
			if not user.check_location(2):
				return bot.send_message(message.chat.id, 'Вы должны находится на локации <b>Мэрия</b> для устройства на работу\nПерейти на локацию: <b>/move</b>', parse_mode='html')

			jobs = User.get_jobs()
			del jobs[0]

			keyboard = types.InlineKeyboardMarkup()
			for job in jobs:
				keyboard.add(types.InlineKeyboardButton(text=jobs[job]['title'], callback_data='getjob_{0}'.format(job)))
			keyboard.add(types.InlineKeyboardButton(text='Отмена', callback_data='getjob_c'))

			bot.send_message(message.chat.id, text='Выберите работу', reply_markup=keyboard)
	@bot.message_handler(commands=['move'])
	def move(message):
		user = User.get_by_t_id(message.from_user.id)
		if user:
			locations = User.get_locations()
			del locations[user.location]

			keyboard = types.InlineKeyboardMarkup()
			for location in locations:
				keyboard.add(types.InlineKeyboardButton(text=locations[location]['title'], callback_data='move_{0}'.format(location)))
			keyboard.add(types.InlineKeyboardButton(text='Отмена', callback_data='move_c'))

			bot.send_message(message.chat.id, text='Выберите локацию', reply_markup=keyboard)

	@bot.message_handler(commands=['work'])
	def work(message):
		user = User.get_by_t_id(message.from_user.id)
		if user:
			func.do_work(bot, message.chat.id, user)

	@bot.message_handler(commands=['info'])
	def info(message):
		users = User.get_for_online()
		user_locations = User.get_locations()

		locations = {}

		for location in user_locations:
			locations[location] = 0

		for user in users:
			locations[user.location] += 1

		info_text = '– Версия <b>SAMPGRAM</b>: <b>{0}</b> –\n'.format(os.environ['APP_VERSION'])
		info_text += 'Общий онлайн: <b>{0}</b>\n\n'.format(len(users))

		for location in locations:
			info_text += '{0}: <b>{1}</b>\n'.format(user_locations[location]['title'], locations[location])

		bot.send_message(message.chat.id, info_text, parse_mode='html')

	@bot.message_handler(commands=['aschool'])
	def aschool(message):
		user = User.get_by_t_id(message.from_user.id)
		if user and user.a_lic == 0:
			if user.cash < 1500:
				return bot.send_message(message.chat.id, 'У Вас недостаточно денег\nНеобходимо: <b>$1500</b>', parse_mode='html')

			user.cash -= 1500
			user.a_lic = 1
			user.commit()

			bot.send_message(message.chat.id, 'Поздравляем!\nВы успешно получили <b>водительское удостоверение</b>!', parse_mode='html')

	@bot.message_handler(commands=['shop'])
	def shop(message):
		user = User.get_by_t_id(message.from_user.id)
		if user:
			shops = get_shops()
			location_shops = {}

			for shop in shops:
				if user.check_location(shops[shop]['location']):
					location_shops[shop] = shops[shop]

			if len(location_shops) < 1:
				return bot.send_message(message.chat.id, 'Магазинов в локации не найдено')

			keyboard = types.InlineKeyboardMarkup()
			for shop in location_shops:
				keyboard.add(types.InlineKeyboardButton(text=location_shops[shop]['title'], callback_data='shop_{0}'.format(shop)))
			keyboard.add(types.InlineKeyboardButton(text='Отмена', callback_data='shop_c'))

			bot.send_message(message.chat.id, text='Выберите магазин', reply_markup=keyboard)

	@bot.message_handler(commands=['stock'])
	def stock(message):
		user = User.get_by_t_id(message.from_user.id)
		job = Job.get_by_job_id(1)
		if user:
			if not user.check_location(3):
				return bot.send_message(message.chat.id, 'Вы должны быть на локации <b>Склад</b>', parse_mode='html')

			stock_price = 175000
			sell_percent = 50
			args = message.text.split()[1:]

			if len(args) < 1:
				if job.owner_id == user.id:
					return bot.send_message(message.chat.id, 'Используйте: /stock <buy|info|sell|sellmat>')
				else:
					return bot.send_message(message.chat.id, 'Используйте: /stock <buy|info>')

			type = args[0]
			if type == 'buy':
				if job.owner_id:
					return bot.send_message(message.chat.id, 'У Склада уже есть владелец')
				if user.cash < stock_price:
					return bot.send_message(message.chat.id, 'У Вас недостаточно денег\nНеобходимо: <b>${0}</b>'.format(stock_price), parse_mode='html')
				user.cash -= stock_price
				user.commit()
				job.owner_id = user.id
				job.commit()

				return bot.send_message(message.chat.id, 'Позравляем, Вы купили Склад за <b>${0}</b>\n'.format(stock_price), parse_mode='html')
			elif type == 'info':
				stock_info_text = 'Сейчас на складе <b>{0}</b> материалов'.format(job.items)
				if job.owner_id:
					owner = User.get(job.owner_id)
					stock_info_text += '\nВладелец: <b>@{0}</b>'.format(owner.t_username)
				else:
					stock_info_text += '\nУ Склада нет владельца'
				return bot.send_message(message.chat.id, stock_info_text, parse_mode='html')
			elif type == 'sell':
				if job.owner_id != user.id:
					return bot.send_message(message.chat.id, 'Вы не владеете Складом')
				sell_cost = round(((stock_price * sell_percent) / 100))

				if '{0}_STOCK_SELL_CONFIRM'.format(message.from_user.id) in os.environ:
					del os.environ['{0}_STOCK_SELL_CONFIRM'.format(message.from_user.id)]
					job.owner_id = None
					job.commit()

					user.cash += sell_cost
					user.commit()
					return bot.send_message(message.chat.id, 'Вы успешно продали склад за <b>${0}</b>'.format(sell_cost), parse_mode='html')

				os.environ['{0}_STOCK_SELL_CONFIRM'.format(message.from_user.id)] = '1'
				return bot.send_message(message.chat.id, 'Вы действительно хотите продать склад за <b>${0}</b>?\nЧтобы подтвердить продажу, <b>введите команду ещё раз</b>'.format(sell_cost), parse_mode='html')
			elif type == 'sellmat':
				if job.owner_id != user.id:
					return bot.send_message(message.chat.id, 'Вы не владеете Складом')
				if len(args) != 2:
					return bot.send_message(message.chat.id, 'Используйте: /stock <sellmat> <count>')
				count = int(args[1])
				if count > job.items:
					return bot.send_message(message.chat.id, 'Не хватает материалов\nМатериалов на складе: <b>{0}</b>'.format(job.items), parse_mode='html')

				cash = count * 20
				job.items -= count
				user.cash += cash
				job.commit()
				user.commit()

				return bot.send_message(message.chat.id, 'Вы продали <b>{0}</b> материалов за <b>${1}</b>'.format(count, cash), parse_mode='html')
