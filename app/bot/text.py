import os, json

from bot import func
from db.user import User

def register_text_handlers(bot):
	@bot.message_handler(content_types=['text'])
	def text_handler(message):
		user = User.get_by_t_id(message.from_user.id)

		if not user:
			if message.text.lower() == 'да':
				if not message.from_user.username:
					username = 'user{0}'.format(message.chat.id)
				else:
					username = message.from_user.username

				new_user = User()
				new_user.t_id = message.from_user.id
				new_user.t_username = username
				new_user.cash = 500
				new_user.level = 1
				new_user.exp = 0
				new_user.job = 0
				new_user.location = 1
				new_user.commit()

				bot.send_message(message.chat.id, 'Аккаунт <b>«{0}» (ID: {1})</b> зарегистрирован!'.format(new_user.t_username, new_user.id), parse_mode='html', reply_markup=json.dumps({
					'hide_keyboard': True
				}))
				location_text = 'Вы сейчас находитесь на локации <b>Автобусный Вокзал</b>\n'
				location_text += 'Вы <b>можете общаться с игроками</b>, которые находятся на той же локации, что и Вы\n'
				location_text += 'Для отправки сообщения игрокам просто введите текст\n'
				location_text += 'Он будет отображен всем игрокам на локации\n'
				location_text += 'Для перехода по локациям используйте <b>/move</b>'

				bot.send_message(message.chat.id, location_text, parse_mode='html')

				job_text = 'Для начала Вам нужно устроиться на работу\n'
				job_text += 'Для этого перейдите на локацию <b>Мэрия ЛС (/move)</b> и введите команду <b>/getjob</b>\n'
				job_text += 'Далее Вам нужно перейти на локацию для работы (например для грузчика это Склад)\n'
				job_text += 'Когда Вы будете на локации, для работы введите команду <b>/work</b>\n'
				job_text += 'Заработанные деньги Вы получите во время <b>PAYDAY</b>, который проходит каждый час\n\n'

				job_text += 'Каждый <b>PAYDAY</b> также выдается опыт, который необходим для повышения уровня\n'
				job_text += 'С каждым новым уровнем Вам будут доступны новые работы\n\n'
				job_text += 'Есть начальные подработки, для которых не нужно устраиваться на работу: <b>Грузчик</b> (локация <b>Склад</b>), <b>Фермер</b> (локация <b>Ферма</b>)'

				bot.send_message(message.chat.id, job_text, parse_mode='html')
			elif message.text.lower() == 'нет':
				bot.send_message(message.chat.id, 'Хорошо.\nНо Вы сможете зарегистрироваться позже :)', parse_mode='html', reply_markup=json.dumps({
					'hide_keyboard': True
				}))
			else:
				bot.send_message(message.chat.id, 'Пожалуйста, введите «Да» или «Нет»')
		else:
			main_text_handler(bot, message, user)

def main_text_handler(bot, message, user):
	handler = '{0}_TEXT_HANDLER'.format(message.from_user.id)
	if not handler in os.environ:
		if message.text[0] != '/':
			if user.location_move == 0:
				users = User.get_for_location(user.location)
				for chat_user in users:
					if chat_user.id != user.id:
						try:
							bot.send_message(chat_user.t_id, '— ({0}) @{1}: {2}'.format(User.get_locations()[chat_user.location]['title'], user.t_username, message.text))
						except:
							print('Blocked')
	else:
		if os.environ[handler] == 'adminmessage':
			if user.admin == 1:
				text = message.text

				del os.environ[handler]
				users = User.get_for_payday()

				for c_user in users:
					try:
						bot.send_message(c_user.t_id, 'Администрация SAMPGRAM:\n\n{0}'.format(text))
					except:
						print('Blocked')
