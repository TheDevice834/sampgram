import os

from telebot import types

from db.user import User
from data.shops import shop_items, shop_types, shops as get_shops

def register_callbacks(bot):
	@bot.callback_query_handler(func=lambda call: 'getjob_' in call.data)
	def getjob_callback(call):
		if call.data == 'getjob_c':
			bot.delete_message(call.message.chat.id, call.message.message_id)
			return False

		user = User.get_by_t_id(call.message.chat.id)
		jobs = User.get_jobs()
		text = ''

		if not user.check_location(2):
			return bot.send_message(message.chat.id, 'Вы должны находится на локации <b>Мэрия</b> для устройства на работу', parse_mode='html')

		for job in jobs:
			if call.data == 'getjob_{0}'.format(job):
				job_found = True

				if user.level < jobs[job]['reqlvl']:
					text = 'Недостаточно опыта!\n\nУ Вас: <b>{0}</b> уровень\nТребуется: <b>{1}</b> уровень'.format(user.level, jobs[job]['reqlvl'])
				else:
					user.job = job
					user.commit()
					text = 'Вы устроились на работу: <b>{0}</b>'.format(jobs[job]['title'])

		bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text=text, parse_mode='html')

	@bot.callback_query_handler(func=lambda call: 'move_' in call.data)
	def getjob_callback(call):
		if call.data == 'move_c':
			bot.delete_message(call.message.chat.id, call.message.message_id)
			return False

		user = User.get_by_t_id(call.message.chat.id)
		if not user:
			return False

		if user.location_move != 0:
			return bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text='Вы ещё не добрались до локации <b>{0}</b>\nОсталось: <b>{1} мин.</b>'.format(User.get_locations()[user.location]['title'], user.location_move), parse_mode='html')

		locations = User.get_locations()
		text = ''

		for location in locations:
			if call.data == 'move_{0}'.format(location):

				if locations[location]['city'] == locations[user.location]['city']:
					move = 5
				else:
					move = 10

				user.location = location
				user.location_move = move
				user.commit()

				bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text='Вы доберетесь до локации <b>{0}</b> через <b>{1} мин.</b>'.format(User.get_locations()[user.location]['title'], user.location_move), parse_mode='html')

	@bot.callback_query_handler(func=lambda call: 'shop_' in call.data)
	def shop_callback(call):
		if call.data == 'shop_c':
			bot.delete_message(call.message.chat.id, call.message.message_id)
			return False

		user = User.get_by_t_id(call.message.chat.id)
		if not user:
			return False

		shops = get_shops()
		text = ''

		for shop in shops:
			if call.data == 'shop_{0}'.format(shop):
				titems = shop_types()[shops[shop]['type']]['items']
				items = shop_items()
				keyboard = types.InlineKeyboardMarkup()
				for titem in titems:
					shop_item = items[titems[titem]['type']][titems[titem]['id']]
					keyboard.add(types.InlineKeyboardButton(text='{0} (${1})'.format(shop_item['title'], shop_item['price']), callback_data='shopi_{0}'.format(titem)))
				keyboard.add(types.InlineKeyboardButton(text='Отмена', callback_data='shopi_c'))

				os.environ['{0}_SHOP'.format(call.message.from_user.id)] = str(shop)
				bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text='Выберите товар', reply_markup=keyboard)

	@bot.callback_query_handler(func=lambda call: 'shopi_' in call.data)
	def shopi_callback(call):
		if call.data == 'shopi_c':
			bot.delete_message(call.message.chat.id, call.message.message_id)
			return False

		user = User.get_by_t_id(call.message.chat.id)
		if not user:
			return False

		shops = get_shops()
		shop = int(os.environ['{0}_SHOP'.format(call.message.from_user.id)])

		titems = shop_types()[shops[shop]['type']]['items']
		items = shop_items()
		for titem in titems:
			if call.data == 'shopi_{0}'.format(titem):
				shop_item = items[titems[titem]['type']][titems[titem]['id']]

				if user.cash < shop_item['price']:
					return bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text='У Вас недостаточно денег\nНеобходимо: <b>${0}</b>'.format(shop_item['price']), parse_mode='html')

				user.cash -= shop_item['price']

				if titems[titem]['type'] == 'smartphones':
					user.smartphone = titems[titem]['id']
				user.commit()

				bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text='Вы купили <b>{0}</b> за <b>${1}</b>'.format(shop_item['title'], shop_item['price']), parse_mode='html')

		del os.environ['{0}_SHOP'.format(call.message.from_user.id)]
