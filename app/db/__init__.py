import os
import sqlalchemy as db

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session

engine = db.create_engine('postgresql+psycopg2://{0}:{1}@{2}/{3}'.format(os.getenv('POSTGRES_USER'), os.getenv('POSTGRES_PASSWORD'), os.getenv('POSTGRES_HOST'), os.getenv('POSTGRES_DB')))
connection = engine.connect()
Base = declarative_base()
session = Session(bind=engine)
