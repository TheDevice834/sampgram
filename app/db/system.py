from db import db, session, Base
from datetime import datetime

class System(Base):
	__tablename__ = 'system'

	id = db.Column(db.Integer, primary_key=True)
	option = db.Column(db.String(150), nullable=False)
	value = db.Column(db.String(150), nullable=False)

	def get(option):
		option = session.query(System).filter(System.option == option).first()
		return option.value

	def set(option, value):
		option = session.query(System).filter(System.option == option).first()
		option.value = value
		option.commit()

	def commit(self):
		session.add(self)
		session.commit()
