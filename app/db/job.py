from db import db, session, Base
from datetime import datetime

class Job(Base):
	__tablename__ = 'jobs'

	id = db.Column(db.Integer, primary_key=True)
	job_id = db.Column(db.Integer, nullable=False)
	items = db.Column(db.Integer, nullable=False)
	owner_id = db.Column(db.Integer)

	def get_by_job_id(id):
		return session.query(Job).filter(Job.job_id == id).first()

	def commit(self):
		session.add(self)
		session.commit()
