from db import db, session, Base
from datetime import datetime

from data.locations import locations
from data.jobs import jobs

class User(Base):
	__tablename__ = 'users'

	id = db.Column(db.Integer, primary_key=True)
	reg_date = db.Column(db.Integer, nullable=False)
	t_id = db.Column(db.Integer, unique=True, nullable=False)
	t_username = db.Column(db.String(120), unique=True, nullable=False)
	cash = db.Column(db.Integer, nullable=False)
	level = db.Column(db.Integer, nullable=False)
	exp = db.Column(db.Integer, nullable=False)
	job = db.Column(db.Integer, nullable=False)
	job_cash = db.Column(db.Integer, default=0, nullable=False)
	job_timestamp = db.Column(db.Integer, default=0, nullable=False)
	location = db.Column(db.Integer, nullable=False)
	location_move = db.Column(db.Integer, default=0, nullable=False)
	last_online = db.Column(db.Integer, nullable=False)
	admin = db.Column(db.Integer, default=0, nullable=False)
	a_lic = db.Column(db.Integer, default=0, nullable=False)
	smartphone = db.Column(db.Integer, default=0, nullable=False)

	def __init__(self):
		self.reg_date = datetime.now().timestamp()
		self.last_online = datetime.now().timestamp()

	def get(id):
		return session.query(User).get(id)

	def get_by_t_username(t_username):
		user = session.query(User).filter_by(t_username=t_username).first()
		if user:
			user.last_online = datetime.now().timestamp()
			user.commit()

		return user

	def get_by_t_id(t_id):
		user = session.query(User).filter_by(t_id=t_id).first()
		if user:
			user.last_online = datetime.now().timestamp()
			user.commit()

		return user

	def check_location(self, location):
		if self.location != location or self.location_move != 0:
			return False
		else:
			return True

	def can_work(self, custom_time = 0):
		current_time = datetime.now().timestamp()
		if custom_time == 0:
			time = User.get_jobs()[self.job]['time']
		else:
			time = custom_time

		if current_time > self.job_timestamp + time:
			return True
		else:
			return round((self.job_timestamp + time) - current_time)

	def get_for_online():
		return session.query(User).filter(User.last_online > (datetime.now().timestamp() - 900)).all()

	def get_for_payday():
		return session.query(User).all()

	def get_for_update_locations():
		return session.query(User).filter(User.location_move != 0).all()

	def get_for_location(location):
		return session.query(User).filter(User.location == location, User.location_move == 0).all()

	def add_exp(self, exp):
		new_exp = self.exp + exp
		max_exp = self.count_max_exp()

		while new_exp >= max_exp:
			max_exp = self.count_max_exp()
			new_exp = new_exp - max_exp
			self.level += 1

		self.exp = new_exp

	def get_jobs():
		return jobs()

	def get_locations():
		return locations()

	def commit(self):
		session.add(self)
		session.commit()

	def delete(self):
		session.delete(self)
		session.commit()

	def count_max_exp(self):
		max_exp = 4 * self.level + 2

		return max_exp
