[0.1.4a]:
— Владелец Склада (работа Грузчика) теперь может продавать свои материалы государству
(команда /stock sellmat <кол-во>. В будущем, можно будет продавать материалы другим игрокам)
— Любой игрок может смотреть кол-во материалов на складе (команда /stock info)
— Если у Склада нет владельца, его можно купить за $175,000, введя команду /stock buy
— Можно продать Склад государству за 50% от стоимость покупки, введя /stock sell
— Теперь для работы Грузчика и Фермера не нужно устраиваться на работу
— Теперь на работе Грузчика и Фермера зарплата выдается сразу, а не во время PAYDAY
— Теперь в отображении сообщений, отправленных ботом, используется HTML-разметка
(например, появился жирный текст)

[0.1.3.1a]:
— Теперь Вы не можете получить опыт во время зарплаты, если заработали меньше $200
— Теперь при переходе на локацию, на которой есть магазины, будет отоброжен их список
— Добавлены типы магазинов
— Исправлена Зарплата (когда хоть один игрок удалял час с ботом, Зарплата не приходила никому)

[0.1.3a]:
— Добавлены команды для Администрации
— Добавлены водительская лицензия и автошкола
(для сдачи нужно перейти на локацию Автошкола и ввести команду /aschool)
— Система магазинов
(чтобы перейти в магазин нужно ввести команду /shop, она покажет доступные Вам магазины на Вашей локации)
— Добавлено 3 магазина 24/7, Drive & Drip (2 шт.) на локациях Мэрия ЛС, Таксопарк ЛС и Автошкола
— Добавлены 2 товара в магазины: Смартфон Meizu M5s и смартфон Xiaomi Redmi 7 Note
Товары магазинов будут пополняться, также будут добавлены типы магазинов (товары в магазине одного типа будут отличаться от товаров магазина другого типа)

[0.1.2.2a]:
— Перебалансированы работы (Грузчик, Фермер)
— К локации Склад добавлена система материалов (в будущем можно будет их продавать)
— При переходе на склад отображается его информация (кол-во материалов, владелец)
