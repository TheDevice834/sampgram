-- Adminer 4.7.2 PostgreSQL dump

DROP TABLE IF EXISTS "jobs";
DROP SEQUENCE IF EXISTS jobs_id_seq;
CREATE SEQUENCE jobs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."jobs" (
    "id" integer DEFAULT nextval('jobs_id_seq') NOT NULL,
    "job_id" integer NOT NULL,
    "items" integer NOT NULL,
    "owner_id" integer,
    CONSTRAINT "jobs_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

TRUNCATE "jobs";
INSERT INTO "jobs" ("id", "job_id", "items", "owner_id") VALUES
(1,	1,	'0',	0);

DROP TABLE IF EXISTS "system";
DROP SEQUENCE IF EXISTS system_id_seq;
CREATE SEQUENCE system_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."system" (
    "id" integer DEFAULT nextval('system_id_seq') NOT NULL,
    "option" character varying(150) NOT NULL,
    "value" character varying(150) NOT NULL,
    CONSTRAINT "system_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

TRUNCATE "system";
INSERT INTO "system" ("id", "option", "value") VALUES
(1,	'payday_exp',	'1'),
(2,	'payday_bonus_cash',	'0');

TRUNCATE "users";
INSERT INTO "users" ("id", "reg_date", "t_id", "t_username", "cash", "level", "exp", "job", "location", "location_move", "job_cash", "job_timestamp", "last_online", "admin", "a_lic", "smartphone") VALUES
(6,	1568373304,	488156071,	'karprog',	248221,	15,	12,	1,	3,	'0',	'0',	1568536727,	1568539176,	1,	1,	2);

-- 2019-09-15 09:22:52.373059+00
